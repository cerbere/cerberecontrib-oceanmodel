Welcome to this contrib for ocean model data.

Installation
============

Requires:
  * cerbere
  * zarr

Examples
========

MITgcm in Zarr format
---------------------

>>> from cerberecontrib_oceanmodel.dataset.mitgcmzarrdataset import MITGCMZarrDataset
>>> dst = MITGCMZarrDataset(
...    '/home/datawork-lops-osi/equinox/mit4320/zarr/',
...    face=10, parameters=['Eta', 'SST']
...    )

>>> cube = GridTimeSeries(dst)
>>> dst = MITGCMZarrDataset(
...    '/home/datawork-lops-osi/equinox/mit4320/zarr/',
...    face=10, parameters=['Eta', 'SST']
...    )

>>> cube = GridTimeSeries(dst)

