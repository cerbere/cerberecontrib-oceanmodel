# -*- coding: utf-8 -*-
"""
Dataset class for MITgcm LL4320 files in zarr format
"""
import logging
from typing import List

import xarray as xr

from cerbere.dataset.dataset import Dataset, OpenMode


class MITGCMZarrDataset(Dataset):
    """Dataset class for MITgcm LL4320 files in zarr format"""

    def _open_dataset(self,
                      face: int,
                      fields: List[str],
                      **kwargs) -> 'xr.Dataset':
        """
        Args:
            face: face number (corresponding to an area) to open
        """
        if self._mode != OpenMode.READ_ONLY:
            raise NotImplementedError
        logging.debug("opening {} in mode : {}".format(self._url, self._mode))

        grid = xr.open_zarr(self.url + 'grid.zarr').sel(face=face)
        ds = xr.merge(
            [xr.open_zarr(self.url + v + '.zarr').sel(face=face)
             for v in fields],
            join='inner')
        ds = ds.assign_coords(**grid.variables)

        # rename coordinates
        ds = ds.rename(
            {'XC': 'lon', 'YC': 'lat', 'j': 'y', 'i': 'x'}
        )

        return ds
