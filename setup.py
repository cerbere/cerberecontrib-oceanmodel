# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for ocean models datasets.

'''

requires = [
    'cerbere>=2.0.0',
    'zarr'
]

setup(
    name='cerberecontrib-oceanmodel',
    version='1.0',
    url='',
    license='GPLv3',
    authors='Jean-François Piollé',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for ocean models',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    entry_points={
        'cerbere.plugins': [
            'MITGCMZarrDataset = cerberecontrib_oceanmodel.dataset.mitgcmzarrdataset:MITGCMZarrDataset',
        ]
    },
    platforms='any',
    packages=find_packages(include=['cerberecontrib_oceanmodel.*']),
    include_package_data=True,
 #   install_requires=requires,
)
